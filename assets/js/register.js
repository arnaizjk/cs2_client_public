//console.log("hello from register")

let registerForm = document.querySelector("#registerUser")


registerForm.addEventListener("submit", (e) => {
	e.preventDefault() 

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let email = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

// console.log(firstName)
// console.log(lastName)
// console.log(email)
// console.log(mobileNo)
// console.log(password1)
// console.log(password2)	

//lets create a validation to enable the submit button when all fields are registered if both passwords match each other
	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {
		
		//if all the requirements are met, lets check for duplicate email inside the database first
			fetch('https://limitless-plains-16333.herokuapp.com/api/users/email-exists', { //why email-exists? to check for duplicate emails
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({ //this is a method that converts a javascript object value into a JSON string
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				//we will now create a control strucutre to see if there are no duplicates found
				if(data === false){ //checking query
					fetch('https://limitless-plains-16333.herokuapp.com/api/users', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1,
							mobileNo: mobileNo
						})
					})
					.then(res => { //adtl note: res.send function sets the content type to text/html which means that the client will now treat the response as text object. it then returns a response to the client 
						return res.json()
					})
					.then(data => {
						console.log(data)
						//lets give a proper response if registration will become successful
						if(data === true){
							alert("New Account Has Registered Successfully")
							//after confirmation of the alert window, lets redirect the user to the login page
							window.location.replace("./login.html")
						} else {
							alert("Something went wrong in the registration")
						}
					})
				}

			})
			
	} else {
		alert("Something went wrong. Check your credentials!")
	}

})