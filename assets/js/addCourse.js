// Course Class: Represents a course
class Course {
  constructor(courseName, coursePrice, courseDescription) {
    this.courseName = courseName;
    this.coursePrice = coursePrice;
    this.courseDescription = courseDescription;
  }
}

// UI Class: Handle UI Tasks
class UI {
  static displayCourses() {
    const courses = Store.getCourses();

    courses.forEach((course) => UI.addCourseToList(course));
  }

  static addCourseToList(course) {
    const list = document.querySelector('#course-list');

    const row = document.createElement('tr');

    row.innerHTML = `
      <td>${course.courseName}</td>
      <td>${course.coursePrice}</td>
      <td>${course.courseDescription}</td>
      <td><a href="./deleteCourse.html" class="btn btn-danger btn-sm delete"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
    `; 

    list.appendChild(row);
  }

  static deleteCourse(el) {
    if(el.classList.contains('delete')) {
      el.parentElement.parentElement.remove();
    }
  }

  static showAlert(message, className) {
    const div = document.createElement('div');
    div.className = `alert alert-${className}`;
    div.appendChild(document.createTextNode(message));
    const container = document.querySelector('.container');
    const form = document.querySelector('#createCourse');
    container.insertBefore(div, form);

    // Vanish in 3 seconds
    setTimeout(() => document.querySelector('.alert').remove(), 3000);
  }

  static clearFields() {
    document.querySelector('#courseName').value = '';
    document.querySelector('#coursePrice').value = '';
    document.querySelector('#courseDescription').value = '';
  }
}

// Store Class: Handles Storage
class Store {
  static getCourses() {
    let courses;
    if(localStorage.getItem('courses') === null) {
      courses = [];
    } else {
      courses = JSON.parse(localStorage.getItem('courses'));
    }

    return courses;
  }


  static addCourse(course) {
    const courses = Store.getCourses();
    courses.push(course);
    localStorage.setItem('courses', JSON.stringify(courses));
  }

  static removeCourse(course) {
    const courses = Store.getCourses();

    courses.forEach((course, index) => {
      if(courses.coursePrice === coursePrice) {
        courses.splice(index, 1);
      }
    });

    localStorage.setItem('courses', JSON.stringify(courses));
  }
}

// Event: Display courses
document.addEventListener('DOMContentLoaded', UI.displayCourses);

// Event: Add a course
document.querySelector('#createCourse').addEventListener('submit', (e) => {
  // Prevent actual submit
  e.preventDefault();

  // Get form values
  const courseName = document.querySelector('#courseName').value;
  const coursePrice = document.querySelector('#coursePrice').value;
  const courseDescription = document.querySelector('#courseDescription').value;

  // Validate
  if(courseName === '' || coursePrice === '' || courseDescription === '') {
    UI.showAlert('Please fill in all fields.');
  } else {
    // Instatiate course
    const course = new Course(courseName, coursePrice, courseDescription);

    // Add course to UI
    UI.addCourseToList(course);

    // Add course to store
    Store.addCourse(course);

    // Show success message
    UI.showAlert('Course Added', 'success');

    // Clear fields
    UI.clearFields();
  }
});

// Event: Remove a course
document.querySelector('#createCourse').addEventListener('submit', (e) => {
  // Remove course from UI
  UI.deleteCourse(e.target);

  // Remove book from course
  Store.removeCourse(e.target.parentElement.previousElementSibling.textContent);

  // Show success message
  // UI.showAlert('Course Removed', 'success');
});