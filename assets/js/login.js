let loginForm = document.querySelector("#loginUser")


loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let email = document.querySelector("#userEmail").value
    let password  = document.querySelector("#password").value   
    // console.log(email)
    // console.log(password)

    //lets now create a control structure that will allow us to determine if there is data inserted in the given fields
    if(email == "" || password == "") {
        alert("Please input your email and/or password")
    } else {
        fetch('https://limitless-plains-16333.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
               email: email,
               password: password 
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            //to check the data has been caught, lets display it first in the console
            console.log(data)
            //upon successful authentication it will return a JWT
            // successful authentication will return a JWT via response accessToken property
            //lets create a control structure to determine if the JWT has been generated together with the user credentials in an object format
            if(data.access){
                //we are going to store JWT in our localStorage
                localStorage.setItem('token', data.access);
                //once that the id is authenticated successfully using the accesstoken then it should redirect the user to the user's profile page 
                //send a fetch request to decode the JWT and obtain the user ID and role for storing inside the context
                 
                    // figure out first
                //  fetch(`http://localhost:3000/api/users/details`, {
                //     headers: {
                //       Authorization: `Bearer ${data.access}`
                //     }
                // })
                // .then(res => {
                //     return res.json()
                // })
                // .then(data => {
                //     //set the global user state to have properties containing authenticated user ID and Role.
                //     localStorage.setItem("id", data._id)
                //     localStorage.setItem("isAdmin", data.isAdmin)
                    // figure out first
                    window.location.replace("./profile.html")                   
            }
                //once that the id and is admin property of the user is succefully saved in the local storage, then the next task is to redirect the user to the profile page. 
                
            else {

                alert("something Went Wrong, Check your Credentials!")

            }
        })
    }

})


